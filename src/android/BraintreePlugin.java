package net.justincredible;

import android.util.Log;
import android.content.Intent;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.DataCollector;
import com.braintreepayments.api.Venmo;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.braintreepayments.api.interfaces.BraintreeResponseListener;
import com.braintreepayments.api.interfaces.PaymentMethodNonceCreatedListener;
import com.braintreepayments.api.interfaces.BraintreeErrorListener;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.braintreepayments.api.models.VenmoAccountNonce;

/**
 *
 */
public final class BraintreePlugin extends CordovaPlugin implements PaymentMethodNonceCreatedListener, BraintreeErrorListener, BraintreeResponseListener {
    private static final String TAG = "BraintreePlugin";
    private CallbackContext _callbackContext;
    // TODO: implement dispatch queue for async ops
    private volatile CallbackContext _deviceDataCallbackContext;
    private volatile CallbackContext _venmoAuthCallbackContext;
    private BraintreeFragment braintreeFragment;
    private String temporaryToken;
    private String deviceData;

    @Override
    public synchronized boolean execute(String action, final JSONArray args, final CallbackContext callbackContext) throws JSONException {
        if (action == null) {
            Log.e(TAG, "execute ==> exiting for bad action");
            return false;
        }
        Log.w(TAG, "execute ==> " + action + " === " + args);
        _callbackContext = callbackContext;

        try {
            if (action.equals("initialize")) {
                this.initialize(args);
            } else if (action.equals("presentDropInPaymentUI")) {
                callbackContext.error("Unsupported Operation: " + action);
            } else if (action.equals("paypalProcess")) {
                callbackContext.error("Unsupported Operation: " + action);
            } else if (action.equals("paypalProcessVaulted")) {
                callbackContext.error("Unsupported Operation: " + action);
            } else if (action.equals("setupApplePay")) {
                callbackContext.error("Unsupported Operation: " + action);
            } else if (action.equals("isVenmoInstalled")) {
                isVenmoInstalled();
            } else if (action.equals("getDeviceData")) {
                if (_deviceDataCallbackContext == null) {
                    _deviceDataCallbackContext = callbackContext;
                    getDeviceData();
                } else {
                    callbackContext.error("Request to retrieve Device Data in progress...");
                }
            } else if (action.equals("authorizeVenmoAccount")) {
                if (_venmoAuthCallbackContext == null) {
                    _venmoAuthCallbackContext = callbackContext;
                    authorizeVenmoAccount();
                } else {
                    callbackContext.error("Request to Venmo Authorize in progress...");
                }
            } else {
                // The given action was not handled above.
                return false;
            }
        } catch (Exception exception) {
            callbackContext.error("BraintreePlugin uncaught exception: " + exception.getMessage());
        }
        return true;
    }

    @Override
    public void onError(Exception error) {
        Log.e(TAG, "Caught error from BraintreeSDK: " + error.getMessage());
        _callbackContext.error("BraintreePlugin uncaught exception: " + error.getMessage());
    }

    // Actions
    private synchronized void initialize(final JSONArray args) throws Exception {
        // Ensure we have the correct number of arguments.
        if (args.length() != 1) {
            _callbackContext.error("A token is required.");
            return;
        }
        // Obtain the arguments.
        String token = args.getString(0);
        if (token == null || token.equals("")) {
            _callbackContext.error("A token is required.");
            return;
        }
        temporaryToken = token;
        try {
            braintreeFragment = BraintreeFragment.newInstance(this.cordova.getActivity(), temporaryToken);
            braintreeFragment.addListener(this);
        } catch (InvalidArgumentException e) {
            // There was an issue with your authorization string.
            Log.e(TAG, "Error creating Authenticating interface: " + e.getMessage());
            _callbackContext.error(TAG + ": Error creating PayPal interface: " + e.getMessage());
        }
        _callbackContext.success();
    }

    private synchronized void authorizeVenmoAccount() throws Exception {
        if (temporaryToken == null || braintreeFragment == null) {
            _callbackContext.error("The Braintree client must first be initialized via BraintreePlugin.initialize(token)");
            return;
        }
        if (!Venmo.isVenmoInstalled(this.webView.getContext())) {
            _callbackContext.error("The Braintree client must be installed for authorization");
            return;
        }
        // Async function
        Venmo.authorizeAccount(braintreeFragment, false);
    }

    private synchronized void getDeviceData() throws JSONException {
        if (temporaryToken == null) {
            _callbackContext.error("The Braintree client must first be initialized via BraintreePlugin.initialize(token)");
            return;
        }
        if (this.deviceData == null || this.deviceData == "") {
            DataCollector.collectDeviceData(braintreeFragment, this);
        } else {
            _callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, this.deviceData));
        }
    }

    private synchronized void isVenmoInstalled() throws JSONException {
        if (temporaryToken == null) {
            _callbackContext.error("The Braintree client must first be initialized via BraintreePlugin.initialize(token)");
            return;
        }
        _callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, Venmo.isVenmoInstalled(this.webView.getContext())));
    }

    private synchronized void setupApplePay() throws JSONException {
        // Apple Pay available on iOS only
        _callbackContext.success();
    }

    // Results

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethodNonce) {
        Log.i(TAG, "onPaymentMethodNonceCreated  ==> paymentMethodNonce = " + paymentMethodNonce);
        if (_venmoAuthCallbackContext == null) {
            Log.e(TAG, "onPaymentMethodNonceCreated exiting ==> callbackContext is invalid");
            return;
        }

        try {
            JSONObject json = new JSONObject();
            json.put("nonce", paymentMethodNonce.getNonce().toString());
            json.put("deviceData", this.deviceData);
            json.put("type", paymentMethodNonce.getTypeLabel());
            json.put("localizedDescription", paymentMethodNonce.getDescription());
            if (paymentMethodNonce instanceof VenmoAccountNonce) {
                VenmoAccountNonce van = (VenmoAccountNonce) paymentMethodNonce;
                JSONObject venmoMap = new JSONObject();
                venmoMap.put("username", van.getUsername());
                json.put("venmoAccount", venmoMap);
            }

            _venmoAuthCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, json));
        } catch (Exception e) {
            Log.e(TAG, "onPaymentMethodNonceCreated  ==> error:" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(Object deviceData) {
        this.deviceData = (String) deviceData;
        _deviceDataCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, this.deviceData));
        _deviceDataCallbackContext = null;
    }
}
